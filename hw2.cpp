/* Becky Pier
CS 110A, Assignment 2
Blackjack program.
She may not look like much but she's got it where it counts.
*/

#include <iostream>
using namespace std;

void playGame();
// This function encapsulates one hand of blackjack. 

int generateCard();
// Generates random number between 1-10.

char hitMe();
// Asks user if they want another card.

int playCard(int running_total);
// Calls generateCard(), calls dealCard(), display card and running total values, returns running total

int dealInitialCards(int running_total, bool isDealer);
// Calls playCard twice and create initial hand.

void displayValue(int running_total);
// Displays value of running total, duh.

int dealCard(int running_total);
// Calls playCard to deal player cards (after initial hand.)

int dealCard(int running_total, bool isDealer);
// Calls playCard to deal dealer cards (after initial hand.) 

void calculateWinner(int player_total, int dealer_total);
// Do I *really* have to have comments on all function prototypes? You can figure this one out, right?

const int SCORE_LIMIT = 21;
const int DEALER_LIMIT = 17;

int main() {
  char play_again;
    do {
      playGame();
      cout << "Would you like to play again? (y/n): ";
      cin >> play_again;
      play_again = tolower(play_again);
    } while (play_again == 'y');
  return 0;
}

/* Here's how the call stack goes:
dealCard OR dealInitialHand
  playCard
    generateCard
  displayValue
*/

// FUNCTION DEFINITIONS:

// ***** generates random number between 1-10.
int generateCard() {
  return (2 + rand() % 9);
}

/* ***** Asks user if they want another card.
Britney Spears would love this function. 
*/
char hitMe() {
  char another_card;
  cout << "Would you like another card? (y/n) :";
  cin >> another_card;
  another_card = tolower(another_card);
  return another_card;
}

/* ***** HOLY FUNCTIONS, BATMAN.
I'm sorry. I wrote some code this summer, really, I swear.
I must have gotten a little rusty, 'cause clearly this 
could have been better designed.
Anyhow, this function is called every time a card is played.
Generates card, updates running total, displays value of card, 
returns running total.
*/
int playCard(int running_total) {
  int card = generateCard();
  running_total = running_total + card;
  cout << "Card: " << card << endl;
  return running_total;
}

// ***** Displays the value of an int. Yep.
void displayValue(int running_total) {
  cout << "Total: " << running_total << endl;
}

/* ***** Calls playCard to deal player cards (after initial hand.)
dealCard and playCard are separate functions so as not to print "total" 
the first time a card is played. 
*/
int dealCard(int running_total) {
  running_total = playCard(running_total);
  cout << "Your ";
  displayValue(running_total);
  return running_total;
}

/* ***** Sneaky overloaded function changes verbiage 
so the player knows whose hand is being referenced.
*/
int dealCard(int running_total, bool isDealer) {
  running_total = playCard(running_total);
  cout << "Dealer's ";
  displayValue(running_total);
  return running_total;
}

/* ***** Deals initial hand.
isDealer changes verbiage so they player knows whose 
hand is being referenced.
*/
int dealInitialCards(int running_total, bool isDealer) {
  const int INITIAL_HAND = 2; // players receive 2 cards initially
  cout << "Here's ";
  if (isDealer == false)
    cout << "your ";
  else
    cout << "the dealer's ";
  cout <<"initial hand..." << endl;
  for (int i = 0; i < INITIAL_HAND; i++) {
      running_total = playCard(running_total);
  }
  displayValue(running_total);
  return running_total;
}

/* ***** Function encapsulates one hand of blackjack.
If player or dealer busts, they lose this hand immediately
and the function returns.
*/
void playGame() {
    srand((unsigned)time(0)); // seeding srand with time value
    int player_total(0), dealer_total(0);
    player_total = dealInitialCards(player_total, false);
    dealer_total = dealInitialCards(dealer_total, true);
    char moar_cards = hitMe();
    while (moar_cards == 'y' && player_total < SCORE_LIMIT) {
      player_total = dealCard(player_total);
      if (player_total > SCORE_LIMIT) {
        cout << "Bust! You lose." << endl;
        return;
      }
    moar_cards = hitMe();
    }
    cout << endl << "Pay close attention to the dealer's hand..." << endl;
    while (dealer_total < 17) {
      dealer_total = dealCard(dealer_total, true);
      if (dealer_total > 21) {
        cout << "House busts. You win." << endl;
        return;
      }
    }
    calculateWinner(player_total, dealer_total);
    return;
  } 

void calculateWinner(int player_total, int dealer_total) {
// This function is only called if neither player or dealer busts.
    if (player_total == dealer_total)
      cout << "Push!" << endl;
    else if (dealer_total > player_total)
      cout << "House wins." << endl;
    else
      cout << "You win!" << endl;
    return;
}
