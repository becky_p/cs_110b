/*
Becky Pier
CS 110B
Practice functions
*/

#include <iostream>
#include <cstdlib>
#define length(a) ( sizeof ( a ) / sizeof ( *a ) )
using namespace std;


// accepts a C-Style string as an argument, and returns the number space characters
int spaceCount(char string[]);

// accepts a double array and its size. Returns the number of negative values
int negativeCount(double array[], int size);

// The function should set all three values to the highest of the values.
void setAllToHighest(int &x, int &y, int &z);

// Accepts two C-Style strings, and returns the number of characters that both have in common
char commonCharacterCount(char str1[], char str2[]);

int main() {
	
	//testing spaceCount
	/*
	char * p;
	char nospace[] = ".";
	char four_space[] = "Your mom has 4 spaces.\0";
	p = four_space;
	spaceCount(p);
	*/

	//testing negativeCount
	double no_neg[] = {3.14, 5, 6.66};
	double one_neg[] = {23.45, -45.67, 88.8, 1.27, 5};
	double two_negs[] = {8.67, 42, -31.81, 720, -43.21};
	int size = length(two_negs);
	negativeCount(two_negs, size);
	//negativeCount();
	return 0;
}


// ***** accepts a C-Style string as an argument, and returns the number space characters
int spaceCount(char string[]) {
	const char SPACE = ' ';
	int count = 0;
	int size = strlen(string);
	for (int i = 0; i < size; i++) {
		if (string[i] == SPACE) {
			count++;
		}
	}
	cout << "The string \"" << string << "\" has " << count << " spaces.\n";
	return count;
}


// ***** accepts a double array and its size. Returns the number of negative values
int negativeCount(double array[], int size) {
	int count = 0;
	for (int i = 0; i < size; i++) {
		if (array[i] < 0) {
			count++;
		}
	}
	cout << "The double array has " << count << " negative number(s) in it.\n";
	return count;
}

/*
// ***** The function should set all three values to the highest of the values.
void setAllToHighest(int &x, int &y, int &z)

// ***** Accepts two C-Style strings, and returns the number of characters that both have in common
char commonCharacterCount(char str1[], char str2[]);
*/