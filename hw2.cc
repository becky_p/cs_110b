/* Becky Pier
// CS 110A, Assignment 2
 Blackjack program. */

#include <iostream>
using namespace std;

//void playGame();

int generateCard();
// Generates random number between 1-10.

char hitMe();
// Asks user if they want another card. Hit me, baby.

int playCard(int running_total);
// Generates card, updates running total, prints card value and running total, returns running total

int dealInitialCards(int running_total, bool isDealer);
// Calls playCard twice to deal initial hand

void displayValue(int running_total);
// Prints value of running total on screen.

int dealCard(int running_total);

int dealCard(int running_total, bool isDealer);

//int calculateFinalScore(int player_total, int dealer_total);

const int SCORE_LIMIT = 21;
const int DEALER_LIMIT = 17;
int main() {
  char playAgain;
  do {
    srand((unsigned)time(0)); // seeding srand with time value
    int player_total(0), dealer_total(0);
    player_total = dealInitialCards(player_total, false);
    dealer_total = dealInitialCards(dealer_total, true);
    char moar_cards = hitMe();
    while (moar_cards == 'y') {
      player_total = dealCard(player_total);
      if (player_total > SCORE_LIMIT) {
        cout << "Bust!" << endl;
        break;
      }
    moar_cards = hitMe();
    }
    
    cout << endl << "Pay close attention to the dealer's hand..." << endl;
    while (dealer_total < 17) {
    dealer_total = dealCard(dealer_total, true);
    }
    //calculateFinalScore(player_total, dealer_total);
    cout << "Would you like to play again? (y/n): ";
    cin >> playAgain;
    playAgain = tolower(playAgain);
  
  } while (playAgain == 'y');
  return 0;
}

int generateCard() {
// generates random number between 1-10.
  return (2 + rand() % 9);
}

char hitMe() {
//Britney Spears would love this function.
  char another_card;
  cout << "Would you like another card? (y/n) :";
  cin >> another_card;
  another_card = tolower(another_card);
  return another_card;
}

int playCard(int running_total) {
  int card = generateCard();
  running_total = running_total + card;
  cout << "Card: " << card << endl;
  return running_total;
}

void displayValue(int running_total) {
  cout << "Total: " << running_total << endl;
}

int dealCard(int running_total) {
  running_total = playCard(running_total);
  cout << "Your ";
  displayValue(running_total);
  return running_total;
}

int dealCard(int running_total, bool isDealer) {
  running_total = playCard(running_total);
  cout << "Dealer's ";
  displayValue(running_total);
  return running_total;
}

int dealInitialCards(int running_total, bool isDealer) {
  const int INITIAL_HAND = 2; // players receive 2 cards initially
  cout << "Here's ";
  if (isDealer == false)
    cout << "your ";
  else
    cout << "the dealer's ";
  cout <<"initial hand..." << endl;
  for (int i = 0; i < INITIAL_HAND; i++) {
      running_total = playCard(running_total);
  }
  displayValue(running_total);
  return running_total;
}

/*int calculateFinalScore(int player_total, int dealer_total) {
  if (player_total > 21) {
    cout 
    (player_total == dealer_total) {
    cout << "Push!" << endl;
    return;
  }
  else if (player_total > */
