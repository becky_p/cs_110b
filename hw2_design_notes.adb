constants needed:
const int MAX_CARDS = 12 // you don't want more than 11 cards, ever.


variables needed:

int player_total: running total to hold the value of the player's hand.
int dealer_total: running total to hold value of the dealer's hand.

functions needed:
int generate_card() - generates random number between 2 and 10.

char hitMe() - asks if the player wants another card, returns y or n.

playCard function will:
- generate card?
- update running total
- print current card value and running total.

Is is better to pass the running total by reference?  Or just pass the current running total by value into the playCard function and return the updated running total?

The reason I'm thinking not to pass the running total by reference is "ugh, pointers."  Which kind of makes me want to do it, just to spite my lazy brain.  But then I need to worry about modularity.  If I pass the player running total by reference, will the dealer running total end up pointed to the same value?  I want to be able to write the function in such a way that changes to the dealer's total do not affect the player total and vice versa.  I'll pass the running total by value, and ask Ashi or Beth how to do the other thing.


generateInitialHand runs the playCard function twice to generate the initial hand.  Main will call this function once for each player.
int generateInitialHand(int running_total) {
	for (int i = 0, i < INITIAL_HAND, i++) {
    playCard(running_total)
  }
}