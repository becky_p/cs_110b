/* Becky Pier
CS 110 B
Assignment: Pointer Tasks
*/
#include <iostream>
using namespace std;

void noNegatives(int *x);

void swap(int *var1, int *var2);

int main() {

	// Create to integer variables named x and y
	int x, y;

	// Create an int pointer named p1
	int *p1;

	// Store the address of x in p1
	p1 = &x;

	// Use p1 to set the value of x to 99
	*p1 = 99;

	// Using cout and x, display the value of x
	cout << "The value of x is " << x << endl;

	// Using cout and the pointer p1, display the value of x
	cout << "The value of pointer p1 is " << *p1 << endl;

	// Store the address of y into p1
	p1 = &y;

	// Use p1 to set the value of y to -300
	*p1 = -300;

	// Create two new variables: an int named temp, and an int pointer named p2
	int temp;
	int *p2;

	// Use temp, p1, and p2 to swap the values in x and y (this will take a few statements)
	temp = *p1;
	p2 = &x;
	*p1 = *p2;
	*p2 = temp;
	cout << "The value of x is " << x << " and the value of y is " << y << " \n";

	// Invoke the noNegatives function twice: once with the address of x as the argument, and once with the address of y
	noNegatives(p1);
	noNegatives(p2);
	// Use p2 to display the values in x and y (this will require both assignment statements and cout statements)
	cout << "The value in x, as pointer to by p2 is " << *p2 << endl;
	p2 = &y;
	cout << "Now p2 is pointer at the address of y. The value in y is " << *p2 << endl;
	// Create an int array with two elements. The array should be named ‘a’
	int a[2];
	// Use p2 to initialize the first element of a with the value of x
	p2 = &x;
	a[0] = *p2;
	// Use p2 to initialize the second element of a with the value of y
	p2 = &y;
	a[1] = y;
	// Using cout, display the address of the first element in a
	cout << "The address of the first element in a is " << &a << endl;
	// Using cout display the address of the second element in a
	cout << "The address of the 2nd element in a is " << &a[1] << endl;

	// Use p1, p2, and temp to swap the values in the two elements of array ‘a’. 
	p1 = &a[0];
	p2 = &a[1];
	temp = a[0];
	*p1 = *p2;
	*p2 = temp;

	// Display the values of the two elements. (The first element should be 99, the second 0).
	const int SIZE = 2;
	for (int i = 0; i < SIZE; i++) {
		cout << "Value of a[" << i << "] is " << a[i] << endl;
	}

	// Invoke your swap function with the addresses of x and y
	swap(x, y);

	// Print their values. (x should be 99, y should be 0).
	cout << "The value pointed to by x is " << x << " and the value pointed to by y is " << y << endl;

	// Invoke your swap function with the address of the two elements in array ‘a’
	swap(a[0], a[1]);

	//print their values. (a[0] should be 0, a[1] should be 99)
	for (int i = 0; i < SIZE; i++) {
		cout << "Value of a[" << i << "] is " << a[i] << endl;
	}
	return 0;
}

// ***** Write a function with the following signature: void noNegatives(int *x).
//The function should accept the address of an int variable. 
// If the value of this integer is negative then it should set it to zero
void noNegatives(int *x) {
	if (*x < 0) {
		*x = 0;
	}
}

// ***** Write a function named ‘swap’ that accepts two integer addresses as arguments, and then swaps the contents of the two integers.
// ***** I'm tempted to be a smarty pants and write this using bitwise NOR, but I'll refrain.
void swap(int *var1, int *var2) {
	int temp = *var1;
	*var1 = *var2;
	*var2 = temp;
}