/* Becky Pier
CS 110B, Assignment 4
C String Functions
*/
#include <iostream>
#include <stdio.h>
#include <cstdlib>
using namespace std;

const int MAX_ARRAY_SIZE = 100; // That's a pretty long phrase there buster.
const int SENTINEL_VALUE = -1; // sentinel value for index functions

// Finds last index where target char can be found in the string.
int lastIndexOf(char s[], char target);

// Reverses the order of any string that is passed in (ex: "Fred" to "derF"
void reverse(char s[]);

// Finds all instances of the char ‘target’ in the string and replace them with ‘replacementChar’. Returns the number of replacements performed.
int repl(char s[], char target, char replacementChar);

// Returns the index in string s where the substring can first be found (or -1 if substring does not appear in string.)
int findsubstring(char s[], char substring[]);

// Returns true if argument string is a palindrome, false if not.
bool isPalindrome(char s[]);

int main() {

	/*TESTING CODE

	// Sample data:
	char odd_palindrome[] = {"rotavator\0"};
	char even_palindrome[] = {"redder\0"};
	// Ten? No bass orchestra tarts, eh? Cross a bonnet!
	char not_palindrome[] = {"flower\0"};
	char target = 'z';
	char replace = 'x';
	char substring[] = "at\0";

	// using a pointer just 'cause I can.
	char *pfoo = &odd_palindrome[0];

	bool yourMom = isPalindrome(pfoo);

	END TESTING CODE */
	return 0;
}

// ***** Finds last index where target char can be found in the string. If target char does not appear in the string, return -1
int lastIndexOf(char s[], char target) {
	int index = SENTINEL_VALUE;
	int size = strlen(s);
	for (int i = 0; i < size; i++) {
		if (s[i] == target) {
			index = i;
		}
	}
	cout << "Last index of " << s << " is " << index << endl;
	return index;
}

// ***** Reverses the order of any string that is passed in (ex: "Fred" to "derF"
void reverse(char s[]) {
	int size = strlen(s);
	cout << "strlen = " << size << endl;
	for (int i = (size-1); i >= 0; i--) {
		cout << s[i];
	}
	cout << endl;
}

// ***** Finds all instances of the char ‘target’ in the string and replace them with ‘replacementChar’. Returns the number of replacements performed.
int repl(char s[], char target, char replacementChar) {
	cout << "Beginning string is " << s << endl;
	int size = strlen(s);
	int num_replacements = 0;
	for (int i = 0; i < size; i++) {
		if (s[i] == target) {
			s[i] = replacementChar;
			num_replacements++;
		}
	}
	if (num_replacements == 0) {
		cout << "Target character not found in string.\n";
		return SENTINEL_VALUE;
	}
	cout << "Altered string is " << s << " with " << num_replacements<< "replacements.\n";
	return num_replacements;
}

// ***** Returns the index in string s where the substring can first be found (or -1 if substring does not appear in string.)
int findsubstring(char s[], char substring[]) {
	int size = strlen(s);
	int sub_size = strlen(substring);
	int index = SENTINEL_VALUE;
	for (int i = 0; i < size; i++) {
		if (s[i] == substring[0]) {
			int j = i + 1;
			for (int k = 1; k < sub_size; j++, k++) {
				if (s[j] == substring[k]) {
					// TESTING cout << "s[" << j << "] == substring[" << k << "]" << endl;
					index = i;
				}
				else {
					index = SENTINEL_VALUE;
					break;
				}
			}
		}
	}
	cout << "index = " << index << endl;
	return index;
}


// ***** Returns true if argument string is a palindrome, false if not.
bool isPalindrome(char s[]) {
	int word_length = strlen(s);
	int k = (word_length / 2);
	for (int i = 0, j = word_length-1; i < k; i++, j--) {
	if (toupper(s[i]) != toupper(s[j])) { //input comparison is case-insensitive
        	cout << "false\n";
        	return false;
    	}
    }
        cout << "true\n";
        return true;
}
