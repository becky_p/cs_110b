#include <iostream>
#include <cmath>
using namespace std;

class Circle 
{
public:
	// default constructor
	Circle();
	// parametrized constructor
	Circle(double r, double x, double y);

	// setters
	void setRadius(double r);
	void setX(double val);
	void setY(double val);

	// getters
	double getRadius();
	double getX();
	double getY();
	double getArea();

	// member functions
	bool containsPoint (double xValue, double yValue);
	
private:
	// data
	double Radius;
	double Center_x;
	double Center_y;

};

// constructors
Circle::Circle()
{

}

Circle::Circle(double r, double x, double y)
{
	Radius = r;
	Center_x = x;
	Center_y = y;
}

// setters
void Circle::setRadius(double r)
{
	Radius = r;
}

void Circle::setX(double val)
{
	Center_x = val;
}

void Circle::setY(double val)
{
	Center_y = val;
}

// getters
double Circle::getRadius()
{
return Radius;
}

double Circle::getX()
{
	return Center_x;
}

double Circle::getY()
{
	return Center_y;
}

double Circle::getArea()
{
	return ((Radius*Radius) * 3.14);
}

// member functions
bool Circle::containsPoint(double xValue, double yValue)
{
	double first = (xValue-Center_x);
	double second = (yValue - Center_y);
	double distance = sqrt((first*first) + (second*second));
	if (distance <= Radius) {
		cout << "The point is inside the circle." << endl;
		return true;
	}
	else {
		cout << "The point is not inside the circle." << endl;
		return false;
	}
}
int main() {

	Circle c;
	
	Circle d(6, 12, 12);
	cout << "The radius of Circle d is " << d.getRadius() << endl;
	cout << "The X-value of circle d is " << d.getX() << endl;
	cout << "The Y-value of circle d is " << d.getY() << endl;
	cout << "The area of circle d is " << d.getArea() << endl;
	
	c.setRadius(5);
	cout << "The radius of Circle c is " << c.getRadius() << endl;
	c.setX(10);
	cout << "The X-value of Circle c is " << c.getX() << endl;
	c.setY(22);
	cout << "The Y-value of Circle c is " << c.getY() << endl;
	c.containsPoint(10,22);

	return 0;
}