/*
Becky Pier
CS 110B
High Scores Assignment. 
*/

#include <iostream>
using namespace std;

int findMaxIndex(int array[], int size);
// returns index of highest value in array

int main() {
	int num_scores;
	cout << "How many scores will you enter? ";
	cin >> num_scores;
	int scores[num_scores];
	char *names[num_scores];
	char **p;
	cin.ignore();
	for (int i = 0; i < num_scores; i++) {
		p = &names[i];
		cout << "Enter the name for scorer #" << (i+1) << ": ";
		char temp[100]; // reading data into temp variable to get size
		cin.getline(temp, 100);
		int length = (strlen(temp) + 1);
		*p = new char[length]; 
		for (int j = 0; j < length; j++) {
			(*p)[j] = temp[j];
		}
		cout << "Enter " << p[0] << "'s score: ";
		cin >> scores[i];
		cin.ignore();
	}

	cout << endl << "TOP SCORERS!" << endl;
	for (int k = 0; k < num_scores; k++) {
		cout << names[k] << ": " << scores[k] << endl;
	}

	int maxIndex = findMaxIndex(scores, num_scores);
	cout << endl << "All-time winner is " << names[maxIndex] << " with a score of " << scores[maxIndex] << endl;
	return 0;
}

// ***** Returns index of highest value in array
int findMaxIndex(int array[], int size) {
	int max = array[0];
	int index = 0;
	for (int i = 0; i < size; i++) {
		if (array[i] > max) {
			max = array[i];
			index = i;
		}
	}
	return index;
}
