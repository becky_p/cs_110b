#include <iostream>
#include <sstream>
using namespace std;

//const int NUM_SQUARES = 9;
class TicTacToeBoard
{
	public:
		// constructor
		TicTacToeBoard();

		// setters
		void setX();
		void setO();

		// getters

		// member functions
		void printBoard();
		void checkWinner(char player);

	private:
		// data
		char board[10];
};

// constructors
TicTacToeBoard::TicTacToeBoard() {
	for (int i = 0; i < 9; i++) {
		board[i] = '_';
	}
}

void TicTacToeBoard::printBoard() {
	for (int i = 0; i < 9; i++) {
		cout << board[i];
		if ((i + 1)  % 3 == 0) {
			cout << endl;
		}
	}
	cout << endl;
}

void TicTacToeBoard::setX() {
	int index;
	cout << "Player X, please enter which square you want: ";
	cin >> index;
	board[index] = 'X';
}

void TicTacToeBoard::setO() {
	int index;
	cout << "Player O, please enter which square you want: ";
	cin >> index;
	board[index] = 'O';
}

void TicTacToeBoard::checkWinner(char player) {
	// should I write a bunch of member functions to check for down, across, diagonal wins? 
	// Or should these be 
	// check for across wins
	for (int i = 0; i < 9; i+=3) {
		if (board[i] == player) {
			for (int j = i, count = 1; j < (i + 3); j++) {
				if (board[j] == player) {
					count++;
				}
				if (count == 3) {
					cout << player << " wins!" << endl;
					return;
				}
			}
		}
	}
}

int main() {
	TicTacToeBoard x;
	x.setX();
	x.setX();
	x.setX();
	x.printBoard();
	x.checkWinner('X');
	return 0;
}

/*

TTTDriver.h
#ifndef TTTDRIVER_H
#define TTTDRIVER_H
ENDIF_....

If the class name isn't camel case, put underscores under the words.

TicTacToeBoard;
TTTDriver driver(board);
	for (int i = -; i < 10; i++) {
		driver.generateXWins();
		cout << driver.getBoard().getDisplayString();
	}

void generateXWins();
void generateOWins();
TicTacToe& getBoard();

what instance variables do we use? What state does the object need to be in in order for these functions to work?

Public: an instance variable to hold reference to the board.
Constructor: set this value to.

*/