#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>

using namespace std;

int generateCard();
// returns a random number between 2 and 9

void generateHand(int hand[]);
// generates an array of 5 random cards

void printCards(int hand[], int size);
// prints values in array. Adds one to index so user can easily understand which card is which.

void printArrayValues(int hand[], int size);
// prints values in array.

void testHands(int no_p[], int one_p[], int two_p[], int three_p[], int str8[], int full_h[], int four_k[]);
// testing function. Tests a bunch of dummy hands of known value.

bool isStraight(int hand[]);
// Determines if a given hand is heterosexual.

void evalHand(int hand[]);
// Determines the value of a given hand.

void inputArray(int hand[]);
// allows user to input their own hand.

bool compareArrays(int array1[], int array2[], int size);
// returns true if arrays are the same, false if not

int countThings(int hand[], int value);
// naming this function countOccurrences caused a compiler error. I am confuse.

// used pointer pfoo while testing function to save me some typing.
void (*pfoo)(int[]) = &evalHand;

const int HAND_SIZE = 5;

int main() {

	cout <<"Welcome to the poker hand evaluator.";

	int hand[HAND_SIZE];
	char choice;
	cout << "Enter 'i' to input your own hand,\n or enter 'r' to generate a random hand. :";
	cin >> choice;
	switch (choice) {
		case 'r':
			generateHand(hand);
			break;
		case 'i':
			inputArray(hand);
			break;
		default:
			cout << "Uh oh. You entered the wrong character and crashed the program. Nice job breaking it, hero.\n";
			return 0;
	}

	sort(hand,(hand+HAND_SIZE+1));
	evalHand(hand);

	// testing code!  
	
	// dummy data for testing functions - one hand of each type:
	int no_pair_hand[HAND_SIZE] = {1, 3, 5, 7, 9 };
	int one_pair_hand[HAND_SIZE] = {1, 2, 2, 3, 4 };
	int two_pair_hand[HAND_SIZE] = {1, 1, 2, 2, 3 };
	int three_kind_hand[HAND_SIZE] = {1, 2, 2, 2, 3};
	int straight_hand[HAND_SIZE] = {1, 2, 3, 4, 5};
	int full_house_hand[HAND_SIZE] = {4, 4, 5, 5, 5};
	int four_kind_hand[HAND_SIZE] = {7, 7, 7, 7, 8}; 
	

	// next line tests function with randomly generated hand:
	//pfoo(hand);

	// Used a pointer to save myself some typing while I test:
	//int *p = &no_pair_hand[0];
	//printArrayValues(p, HAND_SIZE);
	//evalHand(p);

	// next line tests function with one hand of each type:
	//testHands(no_pair_hand, one_pair_hand, two_pair_hand, three_kind_hand, straight_hand, full_house_hand, four_kind_hand);
	
	return 0;
}

// ***** This function was used to test a function with all the dummy data.
void testHands(int no_p[], int one_p[], int two_p[], int three_p[], int str8[], int full_h[], int four_k[]) {
	cout << "No pair hand: ";
	pfoo(no_p);
	cout << endl << "one pair hand: ";
	pfoo(one_p);
	cout << endl << "two pair hand: ";
	pfoo(two_p);
	cout << endl << "three pair hand: ";
	pfoo(three_p);
	cout << endl << " straight hand: ";
	pfoo(str8);
	cout << endl << "full house hand: ";
	pfoo(full_h);
	cout << endl << " four of a kind: ";
	pfoo(four_k);
	cout << endl << endl;
}

// ***** This is where the meat of the program is.
 void evalHand(int hand[]) {
 	
 	// First, test to see if you have a straight.
 	if (isStraight(hand) == true) {
 		cout << "Straight! \n";
 		return;
 	}

 	// if it's not a straight, figure out how many unique values the hand has.
	vector<int> myvector(HAND_SIZE);
	vector<int>::iterator it;
	it = unique_copy(hand,hand+HAND_SIZE,myvector.begin());
	myvector.resize (it - myvector.begin());
	int size = myvector.size();

	// should these constants be global or local?  Does it matter?
	const int UNIQ_SIZE_NOPAIR = 4;
	const int UNIQ_SIZE_2P_OR_3OFKIND = 3;
	const int UNIQ_SIZE_FH_OR_4OFKIND = 2;

	// We can determine if it's 1-pair or no pair hand based solely on number of unique values.

	switch (size) {
		case UNIQ_SIZE_NOPAIR:
		cout << "One pair.\n";
		return;

		case HAND_SIZE:
		cout << "No pair. Your hand kinda sucks. Hope you didn't bet too much.\n";
		return;
	}

	// If it's not a straight, 1-pair, or no pair, create a histogram.

	//creating an array to hold our histogram.
	int occurrences[size];
	//initialize all values in array to 0.
	for (int i = 0; i < size; i++) {
		occurrences[i] = 0;
	}

	int value = myvector[0];
	for (int i = 0; i < size; i++) {
		occurrences[i] = countThings(hand, value);
		value = myvector[i + 1];
	}

	/* printing values before sorting for testing purposes
	cout << "Here's the occurrence array before sorting: \n";
	printArrayValues(occurrences, size); */
	
	sort(occurrences,(occurrences+size)); // fixed bug in this line. Off-by-1 error.

	/* printing values for testing purposes
	cout << "Here's the occurrence array after sorting: \n";
	printArrayValues(occurrences, size); */

	// next, we will compare the occurrence array to test arrays.
	int HISTOGRAM_3OFKIND[UNIQ_SIZE_2P_OR_3OFKIND] = {1, 1, 3};
	int HISTOGRAM_2PAIR[UNIQ_SIZE_2P_OR_3OFKIND] = {1, 2, 2};
	int HISTOGRAM_4OFKIND[UNIQ_SIZE_FH_OR_4OFKIND] = {1, 4};
	int HISTOGRAM_FULLH[UNIQ_SIZE_FH_OR_4OFKIND] = {2, 3};

	if (size == UNIQ_SIZE_FH_OR_4OFKIND) {
		if (compareArrays(occurrences, HISTOGRAM_4OFKIND, size) == true) {
			cout << "4 of a kind! \n";
			return;
		}
		if (compareArrays(occurrences, HISTOGRAM_FULLH, size) == true) {
			cout << "Full house. Not the television show. \n";
			return;
		}
	}
	else if (size == UNIQ_SIZE_2P_OR_3OFKIND) {
		if (compareArrays(occurrences, HISTOGRAM_3OFKIND, size) == true) {
			cout << "3 of a kind. \n";
			return;
		}
		if (compareArrays(occurrences, HISTOGRAM_2PAIR, size) == true) {
			cout << "2 pairs. \n";
			return;
		}
	}
	else {
		cout << "There's been an error. Your hand cannot be evaluated. \n";
		return;
	}
} // FFS, don't delete the bracket when you delete test code above.

// ***** Counts ocurrences of a given value in an array.
int countThings(int hand[], int value) {
	int count = 0;
	for (int i = 0; i < HAND_SIZE; i++) {
		if (hand[i] == value) {
			count++;
		}
	}
	return count;
}

// ***** Reads user input into card array.
void inputArray(int hand[]) {
	cout << "Enter five numeric cards, no face cards. Use 2 - 9.\n";
	for (int i = 0; i < HAND_SIZE; i++) {
		cout << "Card " << i+1 << ": ";
		cin >> hand[i];
	}
	cout << endl;
}

// ***** Generates a random number between 2 and 9.
int generateCard() {
  return (2 + rand() % 8);
}

// ***** Generates hand of 5 random values.
void generateHand(int hand[]) {
	srand((unsigned)time(0)); // seeding srand with time value
	for (int i = 0; i < HAND_SIZE; i++) {
		hand[i] = generateCard();
	}
	printCards(hand, HAND_SIZE);
}

// ***** Prints contents of card array. Index is offset by 1 so as not to confuse user.
void printCards(int hand[], int size) {
	cout << "Here's your cards:" << endl;
	for (int i = 0; i < size; i++) {
		cout << "Card " << i+1 << ": " << hand[i] << endl;
	}
	cout << endl;
}

// ***** Prints contents of array with true index.  I mostly used it for testing.
void printArrayValues(int hand[], int size) {
	cout << "Here's your array values:" << endl;
	for (int i = 0; i < size; i++) {
		cout << "index " << i << ": " << hand[i] << endl;
	}
	cout << endl;
}

// ***** Compares 2 arrays. Returns true if they're equal, false if not.
bool compareArrays(int array1[], int array2[], int size) {
	for (int i = 0; i < size; i++) {
		if (array1[i] != array2[i]) {
			return false;
		}
	}
	return true;
}

// ***** Returns true if hand is a straight, false if not.
bool isStraight(int hand[]) {
	int value = hand[0];
	for (int i = 1; i < HAND_SIZE; i++) {
		if (value + i != hand[i]) {
			return false;
		}
	}
	return true;
}
