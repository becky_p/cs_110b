//  Becky Pier
//  CS 110B, Assignment 1
//  Program outputs a number of astericks, as specified by user input.
#include <iostream>
using namespace std;

int main() {
  int numAstericks;
  char goAgain = 'y';
  while (goAgain != 'n') {
    cout << "How many astericks? :";
    cin >> numAstericks;
    for (int i = 0; i < numAstericks; i++) {
      cout << "*";
    }
    cout << endl << "Moar astericks? (y/n): ";
    cin >> goAgain;
  }
  return 0;
}